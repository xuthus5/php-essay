<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>说说评论系统</title>
    <link rel="stylesheet" href="./layui/css/layui.css">
    <link href='style/css.css' rel='stylesheet'>
    <style>
        .center{
            text-align: center;
        }
        .pull-right {
            float: right !important;
        }
        .pull-left {
            float: left !important;
        }
    </style>
</head>
<body>
<?php
if($_COOKIE['name']):
?>
<ul class="layui-nav center" lay-filter="">
    <li class="layui-nav-item layui-this"><a href="index.php">首页</a></li>
    <li class="layui-nav-item"><a href="admin/index.php">发布中心</a></li>
    <li class="layui-nav-item"><a href="search.php">搜索</a></li>
</ul>
<?php
else:
?>
<ul class="layui-nav center" lay-filter="">
    <li class="layui-nav-item layui-this"><a href="index.php">首页</a></li>
    <li class="layui-nav-item"><a href="login.php">登陆</a></li>
    <li class="layui-nav-item"><a href="reg.php">注册</a></li>
    <li class="layui-nav-item"><a href="search.php">搜索</a></li>
</ul>
<?php
endif;
?>
<div class="layui-col-md12">
    <div class="layui-card">
<!--        <div class="layui-card-header center">在这里输出签名</div>-->
        <div class="layui-card-body center">
            <?php
			echo "<div class='panel-body text-center'>";
		    if(!file_exists('./install/ok.lock')){
		    echo '当前程序尚未安装,请前往&nbsp;&nbsp;==>&nbsp;&nbsp;'."<a href='./install' class='layui-bg-red'>安装</a>";
			}else {
				require('./config/connect.php');
				require('./config/config.php');
				//分页
				$startnum = 0;
				if(!$_GET['page'] || $_GET['page'] == 1){
					$startnum = 0;
				}else{
					$startnum = ($_GET['page'] - 1)*$number;
				}
				$countsql = "select * from article";
				$resultc = $_mysqli->query($countsql);
				$num = ceil($resultc->num_rows/$number);
				$fsql = "SELECT * FROM article ORDER BY times DESC limit $startnum,$number;";
				}
				//内容遍历输出
				$result = $_mysqli->query($fsql);
				$post = array();
				while($getr=mysqli_fetch_array($result)){
					$post[] = $getr;
				}
				foreach ($post as $values){
					$plsql = "SELECT * FROM comment where zid='".$values[id]."' ORDER BY times DESC;";
					$plr = $_mysqli->query($plsql);
					$parr = array();
					while ($plc = mysqli_fetch_array($plr)){
						$parr[] = $plc;
					}
					//输出主体
					echo "<fieldset class='layui-elem-field'>
  <legend><h2>".$values[title]."</h2></legend>
  <div class='layui-field-box'>
    <blockquote class='layui-elem-quote layui-quote-nm'>
  ".$values[content]."<div class='pull-right label label-success'>".$values[lable]."</div>
  <br>—".$values[times]."
</blockquote>
  </div>
  <button type='button' class='btn btn-mini btn-primary pull-left' data-iframe='mod.php?zid=\".$values[id].\"&userid=\".$values[userid].\"' data-toggle='modal'>修改</button><button type='button' class='btn btn-mini btn-primary pull-right' data-iframe='comment.php?zid=\".$values[id].\"' data-toggle='modal'>评论</button><br/><br/><div class='col-xs-1'></div><div class='col-xs-10'>";
                    $lc = 1;
                    foreach ($parr as $plarr){
                        echo '<div class="bg-special divover"><span class="col-xs-2">'.$lc.':'.$plarr['pname'].':</span><span class="col-xs-6">'.$plarr['pcontent'].'</span><span class="col-xs-4">'.$plarr['pmail'].'</span><br/>';
                        $lc++;
                        echo '</div>';
                    }
                    echo "</fieldset>";
				    }
					echo "</div></div></div></div><div class='center'><ul class='pager'>";
					for($i=1;$i<=$num;$i++){
					echo "<li><a href='"."?page=$i'>$i</a></li>";
				    }
					echo "</ul></div>";
?>
<script src="style/jquery.js"></script>
<script src="style/zui.js"></script>
<script src="./layui/layui.js"></script>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
</script>
</body>
</html>