<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>说说评论系统</title>
    <link rel="stylesheet" href="../layui/css/layui.css">
    <style>
        .center{
            text-align: center;
        }
    </style>
</head>
<body>


<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header center">
            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
                <legend>安装程序--数据库配置</legend>
            </fieldset>
        </div>
        <div class="layui-card-body">

            <div class="layui-row">
                <div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
                    <div class="grid-demo grid-demo-bg1"> &nbsp;&nbsp;</div>
                </div>
                <div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
                    <div class="grid-demo grid-demo-bg1">
                        <form class="layui-form layui-form-pane" method="post" action="trans.php">
                            <div class="layui-form-item">
                                <label class="layui-form-label">主机地址</label>
                                <div class="layui-input-inline">
                                    <input type="text" autocomplete="off" class="layui-input" value="localhost" name="db_host">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">数据库名称</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="db_name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">数据库帐号</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="db_user" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <label class="layui-form-label">数据库密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" name="db_pwd" placeholder="请输入密码" autocomplete="off" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <input type="submit" class="layui-btn" value="提交">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="layui-col-xs4 layui-col-sm12 layui-col-md4">
                    <div class="grid-demo grid-demo-bg1">&nbsp;&nbsp;</div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>