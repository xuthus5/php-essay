<!DOCTYPE html>
<html lang="zh-cn">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>验证程序</title>
    <link href="../style/css.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
	<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
	    <div class="panel text-center">
		<div class="panel-heading">
		    <h2>验证程序--数据库验证</h2>
		</div>
		<div class="panel-body">
		    <?php
			if(file_exists('ok.lock')){
			    echo '你已经安装了程序,无需再次安装';
			    exit;
			}else{
			error_reporting(0);
			if($_POST['db_user']){
			$str_conn = '<?php';
			$str_conn .= "\n";
			$str_conn .= '$db_hosts = "'.$_POST[db_host].'";';
			$str_conn .= "\n";
			$str_conn .= '$db_user = "'.$_POST[db_user].'";';
			$str_conn .= "\n";
			$str_conn .= '$db_pwd = "'.$_POST[db_pwd].'";';
			$str_conn .= "\n";
			$str_conn .= '$db_name = "'.$_POST[db_name].'";';
			$str_conn .= "\n";
			$str_conn .= '$web_title= "说说评论系统";';
			$str_conn .= "\n";
			$str_conn .= '$number="5";';
			$write_conn = fopen('../config/config.php', 'w+');
			fwrite($write_conn, $str_conn);
			fclose($write_conn);
			}else{
			    echo '操作失败,请先填写数据后再进行提交'."<br />";
			}
			}
		    ?>
		    <?php
		    require('../config/connect.php');
		    if(!$_mysqli){
			echo '连接数据库失败!!!'."<br />";
			echo '点此返回'."<a href='./' class='btn'>重装</a>";
		    }else{
			echo '数据库初始化成功！！'."<br /><br />请立即前往注册帐号,注册第一个用户!因为本程序默认第一个用户为管理员<br/>";
			echo '点此进入'."<a href='../reg.php' class='btn'>注册</a>||<a href='../' class='btn'>首页</a>";
			$lock = fopen('ok.lock', 'w+');
			fclose($lock);
		    }
		    ?>
		</div> 
		<div class="panel-footer">
            说说评论系统
		</div>
		</div>  
		    </div>
		</div>
	<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
  </body>
</html>


