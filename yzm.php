<?php
    $img = imagecreate(80, 25);
    $color = imagecolorallocate($img, 255, 204, 51);
    imagefill($img, 0, 0, $color);
    $color_write = imagecolorallocate($img, 51, 51, 51);
    $string = rand(1000, 9999);
    imagestring($img, 5, 20, 10, $string, $color_write);
    header('content-type:image/png');
    imagepng($img);
    imagedestroy($img);
    setcookie('yzm',$string, time()+3600);
