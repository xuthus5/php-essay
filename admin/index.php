<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>添加文章</title>
    <link rel="stylesheet" href="../layui/css/layui.css">
    <style>
        .center{
            text-align: center;
        }
    </style>
</head>
<body class="body center">
<ul class="layui-nav center" lay-filter="">
    <li class="layui-nav-item layui-this"><a href="/">首页</a></li>
    <li class="layui-nav-item"><a href="setting.php">设置</a></li>
    <li class="layui-nav-item"><a href="../search.php">搜索</a></li>
</ul>
<form class="layui-form layui-form-pane" action="../result.php" method="post">
    <div class="layui-form-item">
        <label class="layui-form-label">说说标题</label>

        <div class="layui-input-block">
            <input type="text" name="title" autocomplete="off" placeholder="请输入标题" lay-verify="required"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">说说标签</label>

        <div class="layui-input-block">
            <input type="text" name='lable' lay-verify="required" placeholder="请输入标签" autocomplete="off"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-form-text">
        <div class="layui-input-block">
            <textarea id="LAY_demo_editor" placeholder="请输入内容" class="layui-textarea" name="content"></textarea>
        </div>
        <label class="layui-form-label">请输入内容</label>
    </div>
    <div class="layui-form-item">
        <button class="layui-btn" lay-submit="" lay-filter="sub">提交</button>
    </div>
</form>

<script src="../layui/layui.js" charset="utf-8"></script>
<script type="text/javascript">
    layui.use(['form', 'layedit', 'laydate', 'element'], function () {
        var form = layui.form
                , layer = layui.layer
                , layedit = layui.layedit
                , element = layui.element;

        //创建一个编辑器
        var editIndex = layedit.build('LAY_demo_editor');

        //自定义验证规则
        form.verify({
            title: function (value) {
                if (value.length < 5) {
                    return '标题至少得5个字符啊';
                }
            }
            , pass: [/(.+){6,12}$/, '密码必须6到12位']
            , content: function (value) {
                layedit.sync(editIndex);
            }
        });
    });
</script>
</body>
</html>