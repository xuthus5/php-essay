<?php require('../config/config.php');?>
<!DOCTYPE html>
<html lang="zh-cn">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>后台设置-<?php echo $web_title; ?></title>
      <link rel="stylesheet" href="../layui/css/layui.css">
    <link href="../style/css.css" rel="stylesheet">
      <style>
          .center{
              text-align: center;
          }
      </style>
  </head>
  <body>
  <ul class="layui-nav center" lay-filter="">
      <li class="layui-nav-item layui-this"><a href="/">首页</a></li>
      <li class="layui-nav-item"><a href="index.php">发布中心</a></li>
      <li class="layui-nav-item"><a href="../search.php">搜索</a></li>
  </ul>
      <div class="container text-center">
	<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
	    <div class="panel text-center">
		<div class="panel-body">
		    <?php
		    error_reporting(0);
		      require('../config/connect.php');
		      $search_user = "SELECT * FROM user WHERE username='".$_COOKIE['name']."'";
		      $get_r = $_mysqli->query($search_user);
		      $search_get = mysqli_fetch_array($get_r);
		      list($id,$name,$passwd,$times) = $search_get;
		      //设置网站标题
		      //设置网站文章显示条数
		      if($id == 1){
		      echo "<form method='post'>
			   网站名称:<span class='label label-badge label-info'>设置程序名称</span>:<br/><br/>
			   <div class='col-xs-3'></div>
			   <div class='col-xs-6'><input class='form-control' type='text' name='title' value='".$web_title."'></div>
			   <div class='col-xs-3'></div>
			   </br></br></br>
			   文章数目:<span class='label label-badge label-info'>设置首页文章显示数目</span>:<br/><br/>
			   <div class='col-xs-3'></div>
			   <div class='col-xs-6'><input class='form-control' type='text' name='number' value='".$number."'></div>
			   <div class='col-xs-3'></div>
			   </br></br>
			   <input type='submit' value='确定' class='btn btn-danger' name='setting'>
			   </form>
			<br/>";
		      }else{
		      echo '你无权操作，只有管理员才有权限哦';
		      }
		      if($_POST['setting']){
			if($_POST['title'] == ""){
			    echo "<div class='panel'><div class='panel-body text-danger'>状态未改变</div></div>";
			}
			if($_POST['number'] == ""){
			    echo "<div class='panel'><div class='panel-body text-danger'>状态未改变</div></div>";
			}
			if(!empty($_POST['title']) && !empty($_POST['number'])){
			    $strings = file_get_contents("../config/config.php");
			    $pattern_t = "/$web_title/";
			    $replace_t = $_POST['title'];
			    $pattern_n = "/$number/";
			    $replace_n = $_POST['number'];
			    $strings1 = preg_replace($pattern_t, $replace_t, $strings);
			    file_put_contents("../config/config.php", $strings1);
			    $strings2 = preg_replace($pattern_n, $replace_n, $strings);
			    file_put_contents("../config/config.php", $strings2);
			echo "<div class='panel'><div class='panel-body text-danger'>修改成功</div></div>";
			}
			}
		    ?>
		    
		    
		</div>
		<div class="panel-footer">
            说说评论系统
		</div>
	    </div>
	</div>
	<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
    </div>
  </body>
</html>

